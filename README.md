# Ancile Internet Explorer

### About
https://gitlab.com/misc-scripts1/windows/ancile/AncilePlugin_InternetExplorer

Ancile Internet Explorer Allows the disabling of automatic updates to Microsoft's Internet Explorer.

This is a plugin that requires Ancile_Core (https://gitlab.com/misc-scripts1/windows/ancile/Ancile_Core) 

For more information go to https://voat.co/v/ancile